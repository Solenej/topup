import axios from 'axios';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

describe('App', () => {
	test('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(
				<App />,
			div
		);
	});
  test('get the data', () => {
    const data = require("./data.json")
    const request = axios.create().get("https://app.fakejson.com/q/xdOdc9ZF?token=M37SFqOXjnZXOBpUuOCRXA").then(res => {
      expect(res).toEqual(data);
    })
    expect(request).toEqual(Promise.resolve())
  });
});

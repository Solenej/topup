import React, { useEffect, useState } from 'react';
//import axios from 'axios';

import List from './components/List/List';
import PhoneNumberForm from './components/PhoneNumberForm/PhoneNumberForm';
import Button from './components/Button/Button';
import { /* GetDataOuput, */ Country, Operator, OptionType, Product, SelectOption } from './components/List/types';

import logo from './assets/Ding-logo-2014.png'

import './App.css';

// JSON with the data if API not working
const jsonData = require("./data.json");

function App() {
  //const [jsonData, setJsonData] = useState<GetDataOuput>({countries: [], operators: [], products: []}) // Data from the response to the API
  const [options, setOptions] = useState<SelectOption[]>([]) // Options for the list
  const [selectedCountry, setSelectedCountry] = useState<Country>(); // Choosen country
  const [selectedOperator, setSelectedOperator] = useState<Operator>(); // Choosen operator
  const [selectedProduct, setSelectedProduct] = useState<Product>(); // Choosen product
  const [phoneNumber, setPhoneNumber] = useState<string>(); // Phone Number to send top up
  const [isPhoneNumberValidated, setIsPhoneNumberValidated] = useState(false);
  const [isFormCompleted, setIsFormCompleted] = useState(false);

  useEffect(() => {
    // Get the data via the API or JSON file
    //axios.create().get("https://app.fakejson.com/q/xdOdc9ZF?token=M37SFqOXjnZXOBpUuOCRXA")
    //.then(response => setJsonData(response.data));
    // Set the first list
    setOptions(jsonData.countries.map((c: Country) => ({ label: c.name, value: c.iso })))
  }, [])

  /**
   * Function to update the selected option in a list
   * @param option the choosen option
   * @param optionType the type of the option (Country, Operator, Product)
   * @param setSelectedOption the function to save the choosen option
   */
  const onChange = (option: SelectOption | null, optionType: OptionType, setSelectedOption: React.Dispatch<React.SetStateAction<Country | undefined>> | React.Dispatch<React.SetStateAction<Operator | undefined>> | React.Dispatch<React.SetStateAction<Product | undefined>>) => {
    const index = jsonData[optionType].findIndex((value: Country | Operator | Product) => {
      switch (optionType) {
        case OptionType.countries:
          return (value as Country).iso === (option && option.value);
        case OptionType.operators:
          return (value as Operator).id === (option && option.value);
        case OptionType.products:
          return (value as Product).id === (option && option.value);
      }
    })
    setSelectedOption(jsonData[optionType][index])
  }

  return (
    <div className={"Form"}>
      <img src={logo} alt={"logo"} />
      {!isFormCompleted ?
        (<>
          <h1>Please complete the form to top up</h1>
          <List placeholder={"Select a country"} options={options} onChange={(option) => { onChange(option, OptionType.countries, setSelectedCountry); setOptions(jsonData.operators.map((o: Operator) => ({ label: o.name, value: o.id }))) }} />

          {selectedCountry ? (
            <PhoneNumberForm value={phoneNumber} setIsPhoneNumberValidated={setIsPhoneNumberValidated} setPhoneNumber={setPhoneNumber} />
          ) : null}

          {isPhoneNumberValidated ? (
            <List placeholder={"Select an operator"} options={options} onChange={(option) => {
              onChange(option, OptionType.operators, setSelectedOperator);
              const productIndex = jsonData.products.findIndex((value: Product) => { return value.id === (option && option.value) })
              // Set the list for the products according to the previous choosen operator
              setOptions(jsonData.products[productIndex].products.map((p: string) => ({ label: p, value: jsonData.products[productIndex].id })))
            }} />

          ) : null}
          {selectedOperator ?
            (
              <>
                <List placeholder={"Select a product"} options={options} onChange={(option) => onChange(option, OptionType.products, setSelectedProduct)} />
                {selectedProduct ? (<Button onClick={() => setIsFormCompleted(true)} value={"Send"} />) : null}
              </>
            ) : null}
        </>) :
        (<span> Congratulation ! You successfully topped up to <strong>{phoneNumber}</strong> </span>)
      }
    </div>
  );
}

export default App;

import React, { FunctionComponent } from 'react';

import './Button.scss';

export type ButtonProps = {
    onClick: () => void;
    value: string;
};

/**
 * Component showing a select with a Button of items
 */
const Button: FunctionComponent<ButtonProps> = ({onClick, value}) => {

    return (
        <button className={"Button"} onClick={() => onClick()}>{value}</button>
    );
};

export default Button;

import React from 'react';
import ReactDOM from 'react-dom';
import List from './List';

describe('List', () => {
	test('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(
			<List onChange={() => {}} options={[]} placeholder={"test"} />,
			div
		);
	});
});

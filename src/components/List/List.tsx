import React, { FunctionComponent } from 'react';
import Select from "react-select";

import { SelectOption } from './types';

import './List.scss';

export type ListProps = {
    options: SelectOption[];
    placeholder: string;
    onChange: (option: SelectOption | null) => void
};

/**
 * Component showing a select with a list of items
 */
const List: FunctionComponent<ListProps> = ({options, placeholder, onChange}) => {

    return (
        <Select className={"List"} placeholder={placeholder} options={options} onChange={(option) => onChange(option)} />
    );
};

export default List;

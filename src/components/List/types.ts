export type GetDataOuput = {
    countries: Country[];
    operators: Operator[];
    products: Product[];
}

export enum OptionType {
    countries = "countries",
    operators = "operators",
    products = "products"
}

export type Country = {
    name: string;
    iso: string;
    prefix: string;
}

export type Operator = {
    name: string;
    iso: string;
    id: string;
}

export type Product = {
    id: string;
    products: string[];
}

export type SelectOption = {
    label: string;
    value: string;
}
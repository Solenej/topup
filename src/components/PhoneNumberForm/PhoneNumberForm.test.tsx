import React from 'react';
import ReactDOM from 'react-dom';
import PhoneNumberForm from './PhoneNumberForm';

describe('PhoneNumberForm', () => {
	test('renders without crashing', () => {
		const div = document.createElement('div');
		ReactDOM.render(
			<PhoneNumberForm setIsPhoneNumberValidated={() => {}} setPhoneNumber={() => {}} value={"test"} />,
			div
		);
	});
});

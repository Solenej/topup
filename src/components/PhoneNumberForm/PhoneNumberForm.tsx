import React, { FunctionComponent, useState } from 'react';
import cx from 'classnames';

import Button from '../Button/Button';

import './PhoneNumberForm.scss';

export type PhoneNumberFormProps = {
    setIsPhoneNumberValidated: React.Dispatch<React.SetStateAction<boolean>>;
    setPhoneNumber: React.Dispatch<React.SetStateAction<string | undefined>>;
    value?: string;
};

/**
 * Component showing a form to enter a phone number
 */
const PhoneNumberForm: FunctionComponent<PhoneNumberFormProps> = ({ setIsPhoneNumberValidated, setPhoneNumber, value }) => {
    const [isPhoneNumberFormatUncorrect, setIsPhoneNumberFormatUncorrect] = useState<Boolean>();

    /**
     * Function to check if the format of the phone number is valid
     */
    const onValidateFormClick = () => {
        if (value && /[0-9]{10}/.test(value)){
            setIsPhoneNumberFormatUncorrect(false)
            setIsPhoneNumberValidated(true);
        } else {
            setIsPhoneNumberFormatUncorrect(true)
        }
    }

    return (
        <div className={"PhoneNumberForm"}>
            <label>Enter the phone number : </label>
            <input className={cx({isPhoneNumberFormatUncorrect})} type={"tel"} value={value} onChange={(evt) => setPhoneNumber(evt.target.value)} required pattern={"[0-9]{10}"} />
            <span className={cx({Visible: isPhoneNumberFormatUncorrect}, "Hidden")}>Phone number format invalide, awaited format : 1234567890</span>
            <Button onClick={() => onValidateFormClick()} value={"Enter"}/>
        </div>
    );
};

export default PhoneNumberForm;
